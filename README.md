
### 小米即时消息云 MIMC uni-app SDK

> 编辑器 HbuilderX
>
> 版本：2.8.3 alpha v3


[mimc-uniapp-sdk-demo]: https://gitee.com/fulusco/mimc-uniapp-sdk-demo



* v3
* "usingComponents" : true
* 支持app



> 基于 mimc-web-sdk （2019年6月17日18:49:35）

> 出现BUG请提交Issues：https://gitee.com/fulusco/mimc-uniapp-sdk/issues

### 使用

> /code/mimc-uniapp-sdk-min.js
>
> 或
>
> /sdk/mimc-uniapp-sdk-min.js

```javascript
// 请查看 demo
// https://gitee.com/fulusco/mimc-uniapp-sdk-demo
```

### 功能测试

> 只测试了消息接收，（H5、微信小程序、安卓）

使用 /demo中的demo.html 发送消息， app接收消息（uniapp程序需要自己写  https://gitee.com/fulusco/mimc-uniapp-sdk-demo）



### 开发

* Python2.7

> /code/mimc-uniapp-sdk-min.js		打包后的文件

> /code/generate_mimc_min_js.bat	重新打包（编辑路径）



#### 开发环境

- phyton2.7，其他版本不知道是否可以正确打包，3.7打包失败