goog.provide('goog.custom');

/**
 * 自定义 window
 */
goog.custom._window = function() {
	// 默认
	var _window = {
		navigator :{
			userAgent: "mimc-uniapp/1.0",
			appName: "xiaomi",
			appVersion: "1.0"
		}
	}
	
//	//#ifdef H5 || MP-WEIXIN
//	_window = window
//	//#endif
//
//	//#ifdef APP-PLUS
//	var _navigator = {
//		userAgent: "mimc-uniapp/1.0/ID:" + plus.device.uuid.toString(), // 设备唯一信息
//		appName: "xiaomi-mimc",
//		appVersion: "1.0"
//	}
//	_window.navigator = _navigator
//	//#endif

	return _window
}